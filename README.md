# C projects

В данной папке располагаются проекты выполненные на языке С в период обучения
в Школе 21 (г. Казань).

Перечень проектов:
- libft
- get_next_line
- minitalk
- so_long
- push_swap
- philosophers
- minishell
- cub3d