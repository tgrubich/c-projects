#include "libft.h"

void	*ft_memmove(void *dest, const void *s, size_t n)
{
	size_t	i;

	if (dest == NULL && s == NULL)
		return (NULL);
	if ((unsigned char *)s < (unsigned char *)dest)
	{
		i = n;
		while (i > 0)
		{
			i--;
			((unsigned char *)dest)[i] = ((unsigned char *)s)[i];
		}
	}
	else
	{
		i = 0;
		while (i < n)
		{
			((unsigned char *)dest)[i] = ((unsigned char *)s)[i];
			i++;
		}
	}
	return (dest);
}
