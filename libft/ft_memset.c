#include "libft.h"

void	*ft_memset(void *dest, int a, size_t b)
{
	char	*i;

	i = (char *)dest;
	while (b > 0)
	{
		*i = a;
		b--;
		i++;
	}
	return (dest);
}
