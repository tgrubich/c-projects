#include "libft.h"

void	*ft_calloc(size_t num, size_t size)
{
	char			*str;
	unsigned int	i;
	unsigned int	prod;

	i = 0;
	prod = num * size;
	str = malloc(prod);
	if (!str)
		return (0);
	while (prod)
	{
		str[i] = 0;
		i++;
		prod--;
	}
	return (str);
}
