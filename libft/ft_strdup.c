#include "libft.h"

char	*ft_strdup(const char *str)
{
	int		i;
	int		j;
	char	*s1;

	i = 0;
	j = 0;
	while (str[i])
		i++;
	s1 = (char *)malloc(sizeof(*str) * i + 1);
	if (!s1)
		return (0);
	while (str[j])
	{
		s1[j] = str[j];
		j++;
	}
	s1[j] = '\0';
	return (s1);
}
