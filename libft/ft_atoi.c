#include "libft.h"

int	ft_atoi(const char *s)
{
	unsigned int	res;
	int				i;
	int				k;

	k = 1;
	i = 0;
	res = 0;
	while (s[i] == ' ' || s[i] == '\t' || s[i] == '\f'
		|| s[i] == '\r' || s[i] == '\n' || s[i] == '\v')
		i++;
	if (s[i] == '+' || s[i] == '-')
		if (s[i++] == '-')
			k = -1;
	while (s[i] >= '0' && s[i] <= '9')
	{
		res = res * 10 + (s[i] - '0');
		i++;
	}
	return ((int)(k * res));
}
