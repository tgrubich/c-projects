#include "libft.h"

char	*ft_strcpy(char *dst, const char *src)
{
	size_t	i;

	i = 0;
	while (src[i])
	{
		dst[i] = src[i];
		i++;
	}
	dst[i] = '\0';
	return (dst);
}

char	*ft_boardnum(const char *s)
{
	char	*dest;
	int		size;

	size = ft_strlen(s);
	dest = (char *)malloc(sizeof(*s) * (size + 1));
	if (dest)
	{
		ft_strcpy(dest, s);
		return (dest);
	}
	else
		return (0);
}

char	*ft_strtointmax(int a, int size)
{
	char	*s;

	s = (char *)malloc(sizeof(*s) * (size + 1));
	if (s)
	{
		s[size] = '\0';
		size--;
		while (size >= 0)
		{
			s[size] = ((a % 10) + '0');
			a = a / 10;
			size--;
		}
		return (s);
	}
	return (0);
}

char	*ft_strtointmin(int n, int size)
{
	char	*str;

	str = (char *)malloc(sizeof(*str) * (size + 2));
	if (str)
	{
		size++;
		str[size] = '\0';
		size--;
		str[0] = '-';
		while (size > 0)
		{
			str[size] = ((n % 10) + '0');
			n = n / 10;
			size--;
		}
		return (str);
	}
	return (0);
}

char	*ft_itoa(int n)
{
	char	*str;
	int		size;
	int		c;

	c = 0;
	size = n;
	if (n == -2147483648)
		return (ft_boardnum("-2147483648"));
	if (n == 0)
		c = 1;
	else
	{
		while (size != 0)
		{
			size = size / 10;
			c++;
		}
	}
	if (n >= 0)
		return (str = ft_strtointmax(n, c));
	return (str = ft_strtointmin((n * (-1)), c));
}
