#include "libft.h"

void	ft_bzero(void *str, size_t k)
{
	size_t	i;

	i = 0;
	while (i < k)
	{
		((char *) str)[i] = 0;
		i++;
	}
}
