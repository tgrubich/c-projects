#include "libft.h"

void	*ft_memchr(const void *mas, int a, size_t n)
{
	size_t	i;

	i = 0;
	while (i < n)
	{
		if (*((unsigned char *)mas + i) == (unsigned char)a)
		{
			return ((unsigned char *)mas + i);
		}
		i++;
	}
	return (0);
}
