#include "libft.h"

char	*ft_strncpy(char *dest, const char *src, size_t num)
{
	size_t	i;

	i = 0;
	if (num == 0)
		return (dest);
	while (num)
	{
		if (!(*src))
			dest[i] = '\0';
		else
		{
			dest[i] = *src;
			src++;
		}
		i++;
		num--;
	}
	return (dest);
}

char	*ft_strtrim(char const *s1, char const *set)
{
	size_t	i;
	size_t	j;
	char	*str;

	i = 0;
	if (!s1 || !set)
		return (0);
	while (s1[i] && ft_strchr(set, s1[i]))
		i++;
	j = ft_strlen(s1 + i);
	if (j)
	{
		while (s1[j + i - 1] != 0 && ft_strchr(set, s1[j + i - 1]) != 0)
			j--;
	}
	str = malloc(sizeof(char) * j + 1);
	if (!str)
		return (0);
	ft_strncpy(str, s1 + i, j);
	str[j] = '\0';
	return (str);
}
