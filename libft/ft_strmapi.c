#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*str;
	size_t	i;
	int		size;

	i = 0;
	if (s == 0)
		return (0);
	size = ft_strlen(s);
	str = (char *)malloc(sizeof(*s) * (size + 1));
	if (!str)
		return (0);
	else
	{
		while (s[i])
		{
			str[i] = f(i, s[i]);
			i++;
		}
		str[i] = '\0';
	}
	return (str);
}
