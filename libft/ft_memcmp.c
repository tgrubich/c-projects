#include "libft.h"

int	ft_memcmp(const void *mas1, const void *mas2, size_t n)
{
	if (!n)
		return (0);
	while ((*(unsigned char *)mas1 == *(unsigned char *)mas2) && --n)
	{	
		mas1 = (unsigned char *)mas1 + 1;
		mas2 = (unsigned char *)mas2 + 1;
	}
	return (*((unsigned char *)mas1) - *((unsigned char *)mas2));
}
