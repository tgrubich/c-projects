#include "libft.h"

void	*ft_memcpy(void *dest, const void *s, size_t n)
{
	unsigned int	i;

	i = 0;
	if (dest == NULL && s == NULL)
		return (NULL);
	while (i < (unsigned int)n)
	{
		((char *)dest)[i] = ((char *)s)[i];
		i++;
	}
	return (dest);
}
