#include "libft.h"

void	*ft_memccpy(void *dest, const void *s, int a, size_t n)
{
	size_t	i;

	i = 0;
	while (i < n)
	{
		((unsigned char *)dest)[i] = ((unsigned char *)s)[i];
		if (((unsigned char *)s)[i] == (unsigned char)a)
		{
			return (&dest[i + 1]);
		}
		else
		{
			if (s == '\0')
			{
				return (0);
			}
		}
		i++;
	}
	return (0);
}
