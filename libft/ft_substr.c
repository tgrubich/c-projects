#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	size_t	i;
	size_t	j;
	char	*newstr;

	if (!s || len <= 0 || start >= ft_strlen(s))
		return (ft_strdup(""));
	else
	{
		j = ft_strlen(&s[start]);
		if (j < len)
			len = j;
		newstr = malloc((len + 1) * sizeof(char));
		if (!(newstr))
			return (NULL);
		i = start;
		while (s[i] && (i - start) < len)
		{
			newstr[i - start] = s[i];
			i = i + 1;
		}
		newstr[i - start] = '\0';
		return (newstr);
	}
}
