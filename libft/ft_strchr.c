#include "libft.h"

char	*ft_strchr(const char *str, int c)
{
	char	*s2;

	s2 = (char *)str;
	while (*s2 != (unsigned char)c)
	{
		if (*s2 == '\0')
		{
			return (0);
		}
		s2++;
	}
	return (s2);
}
